package edu.unomaha.ztbrown;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ViewList
 */
@WebServlet("/ViewList")
public class ViewList extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String url = "jdbc:mysql://54.191.173.193:3306/testDB";
	static String user = "test";
	static String password = "WlOgTs2pXGk9FVXK";
	static Connection connection = null;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type = request.getParameter("type");
		String selectSQL = "";
		if(type == null || type.equals("todo")) {
			selectSQL = "SELECT * FROM TASKS WHERE COMPLETED=0";
		} else if (type.equals("dateASC")){
			selectSQL = "SELECT * FROM TASKS WHERE COMPLETED=0 ORDER BY DUEON";
		} else if (type.equals("dateDSC")){
			selectSQL = "SELECT * FROM TASKS WHERE COMPLETED=0 ORDER BY DUEON DESC";
		} else if (type.equals("complete")){
			selectSQL = "SELECT * FROM TASKS WHERE COMPLETED=1";
		}
		
		response.getWriter().println("<html>");
		response.getWriter().println("<a href=\"AddItem\">Add Item     </a><br><br>");
		response.getWriter().println("<a href=\"ViewList?type=complete\">Show Complete</a><br>");
		response.getWriter().println("<a href=\"ViewList?type=todo\">Show ToDo</a><br><br>");
		response.getWriter().println("<a href=\"ViewList?type=dateASC\">Sort by priority</a><br>");
		response.getWriter().println("<a href=\"ViewList?type=dateDSC\">Sort by reverse priority</a><br><br>");
		response.getWriter().println("<table border=\"1\" cellpadding=\"8\" cellspacing=\"8\">");
		response.getWriter().println("<tr><th>Task Name</th><th>Task Description</th><th>Due Date</th><th>Mark Done</th></tr>");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(url, user, password);
			PreparedStatement preparedStatement = connection.prepareStatement(selectSQL);
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				int ID = rs.getInt("ID");
				String taskName = rs.getString("TITLE");
				String taskDesc = rs.getString("DESCRIPTION");
				String dueDate = rs.getString("DUEON");
				int complete = rs.getInt("COMPLETED");
				if(type != null && type.equals("complete")){
					response.getWriter().println("<tr><td>"+taskName+"</td><td>"+taskDesc+"</td><td>"+dueDate+"</td><td><a href=\"MarkDone?id="+ID+"&done=false\">Mark Not Done</a></td></tr>");
				} else {
					response.getWriter().println("<tr><td>"+taskName+"</td><td>"+taskDesc+"</td><td>"+dueDate+"</td><td><a href=\"MarkDone?id="+ID+"\">Mark Done</a></td></tr>");
			
				}
			}
		} catch (Exception e){
			e.printStackTrace();
		}
		response.getWriter().println("</table></html>");


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
