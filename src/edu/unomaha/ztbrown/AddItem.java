package edu.unomaha.ztbrown;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddItem
 */
@WebServlet("/AddItem")
public class AddItem extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static String url = "jdbc:mysql://54.191.173.193:3306/testDB";
	static String user = "test";
	static String password = "WlOgTs2pXGk9FVXK";
	static Connection connection = null;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter printWriter  = response.getWriter();
		printWriter.println("<h1>Add Item:</h1><br><br>");
		printWriter.println("<html><body><form action=\"AddItem\" method=\"POST\">Item Name: <input type=\"text\" "
				+ "name=\"itemName\"><br><br>Item Description: <input type=\"text\" name=\"itemDesc\" /><br><br>Due Date:"
				+ " <input type=\"date\" name=\"itemDate\" max=\"2100-12-31\" /><br><br><input type=\"submit\" value=\"Submit\" "
				+ "/><input type=\"reset\"></form></body></html>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String itemName = request.getParameter("itemName");
		String itemDesc = request.getParameter("itemDesc");
		String dueDate = request.getParameter("itemDate");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(url, user, password);
			String selectSQL = "INSERT INTO TASKS (TITLE,DESCRIPTION,DUEON,COMPLETED) VALUES (?,?,?,0)";
			PreparedStatement preparedStatement = connection.prepareStatement(selectSQL);
			preparedStatement.setString(1, itemName);
			preparedStatement.setString(2, itemDesc);
			preparedStatement.setString(3, dueDate);
			int rs = preparedStatement.executeUpdate();
		} catch (Exception e) {
			response.getWriter().println("There was an error adding that task...");
			e.printStackTrace();
		}
		response.getWriter().println("<html>Success! <br><br> <a href=\"AddItem\">Add Another Item</a><br><br><a href=\"ViewList\">View List</a></html>");
	}
	
	

}
