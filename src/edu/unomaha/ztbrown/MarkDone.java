package edu.unomaha.ztbrown;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MarkDone
 */
@WebServlet("/MarkDone")
public class MarkDone extends HttpServlet {
	static String url = "jdbc:mysql://54.191.173.193:3306/testDB";
	static String user = "test";
	static String password = "WlOgTs2pXGk9FVXK";
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MarkDone() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ID = request.getParameter("id");
		int itemID;
		if(ID == null) {
			response.getWriter().println("<html><meta http-equiv=\"refresh\" content=\"0; URL='ViewList'\" /></html>");
			return;
		}
		
		try { 
			itemID = Integer.parseInt(ID);
		} catch (Exception e){
			//fail silent
			response.getWriter().println("<html><meta http-equiv=\"refresh\" content=\"0; URL='ViewList'\" /></html>");
			e.printStackTrace( );
			return;
		}
		
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = null;
			connection = DriverManager.getConnection(url, user, password);
			String updateSQL;
			if(request.getParameter("done") == null){
				updateSQL = "UPDATE TASKS SET COMPLETED=1 WHERE ID=?";
			} else {
				updateSQL = "UPDATE TASKS SET COMPLETED=0 WHERE ID=?";
			}
			
			PreparedStatement preparedStatement = connection.prepareStatement(updateSQL);
			preparedStatement.setInt(1, itemID);
			System.out.println(preparedStatement);
			int rs = preparedStatement.executeUpdate();
		} catch (Exception e) {
			response.getWriter().println("There was an error marking that task done...");
			e.printStackTrace();
			return;
		}
		
		
		
		response.getWriter().println("<html><meta http-equiv=\"refresh\" content=\"0; URL='ViewList'\" /></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
